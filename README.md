# Visable Bank
> This is a simple API backend written for Visable Bank. It performs simple CRUD operations on Accounts and transfers as well.
> I used the Money gem to handle currency and monetary operations. Money gem performs operation on the cents level for simplicity and
> handling rounding errors. It also provides localization and formatting. Testing is done with RSpec, for more information, head to the
> testing section.

## Setup
### Manual setup
System requirements:
```
ruby / ruby-on-rails
postgresql
rails
```
These requirements need to be installed first.

- Creating a database: ```createuser -U visable_bank_user -W ```. Next, add 'Asdfgh12!' in the password field.

- Create user database: ```createdb visable_bank_user -O visable_bank_user```

- Setup database: ```rails db:setup```

- run the server: ```rails s```

And you're done.

## API setup

Endpoints
---------

- Fetch all accounts: ```GET /api/v1/accounts```, returns a list of accounts.
- Fetch an account: ```GET /api/v1/accounts/:id```, returns an account and associated transaction history.
- Create an account:
	- ```POST /api/v1/accounts```
	- payload
	```json
		{
			"holder": "Joe Jones 1",
			"balance": "25"
		}
	```
	- on success, returns the newly created account.

- Update an account:
	- ```PUT /api/v1/accounts/:id```
	- payload
	```json
		{
			"holder": "Joseph Jones"
		}
	```
	- on success, returns the updated account.

- Delete an account:
	- ```DELETE /api/v1/accounts/10```, returns only an http status of 204.

- Transfer from an account:
	- ```POST /api/v1/accounts/1/transfer```
	- payload
	```json
		{
			"details": {
				// reciever account number
				"reciever": "47017775",
				// amount in euros
				"amount": 50
			}
		}
	```
	- on success, returns a success message, on failure it responds with the appropriate error response json.


## Testing

As mentioned above, RSpec is heavily used to test all components. We also have integration test (aka requests).
I used Shoulda-matchers for one line matches, database cleaner for cleaning database after each test, json-schema for
validating response json, Factory bot for generating model fixtures and faker to generate fake data. Test coverage seems like
an unnecessary to include in this toy project.

To Run tests:
```bundle exec rspec```

## Misc/Code health

I used the widely used rubocop linter (rubocop-rails in this case). It performs linting and enforce coding style.
