# frozen_string_literal: true

require "rails_helper"

RSpec.describe Transaction, type: :model do
  it { should validate_presence_of(:reciever) }
  it { should validate_presence_of(:amount) }
end
