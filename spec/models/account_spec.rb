# frozen_string_literal: true

require "rails_helper"

RSpec.describe Account, type: :model do
  it { should validate_presence_of(:holder) }
  it { should validate_presence_of(:balance) }
  it { should have_many(:transactions) }

  describe ".send_to" do
    context "with transfer amount greater than account balance" do
      subject { create(:account) }
      let(:reciever) { create(:account) }
      let(:amount) { subject.account_balance.to_i + 1 }

      it "fails" do
        expect { subject.send_to reciever.account_number, amount }.to raise_exception ArgumentError
      end
    end

    context "with a valid account number and transfer amount" do
      subject { create(:account) }
      let(:reciever) { create(:account) }
      let(:amount) { (0.5 * subject.account_balance.to_i).to_i }

      it "succeeds" do
        # expect {}.not_to raise_exception
        expect {
          subject.send_to reciever.account_number, amount
        }.to have_enqueued_job
        # change {
        #   ActiveJob::Base.queue_adapter.enqueued_jobs.count
        # }.by 1
        expect(subject.transactions).not_to be_empty
      end
    end
  end
end
