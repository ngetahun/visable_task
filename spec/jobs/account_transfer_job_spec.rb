# frozen_string_literal: true

require "rails_helper"

RSpec.describe AccountTransferJob, type: :job do
  describe "#perform_later" do
    let(:sender) { create(:account) }
    let(:reciever) { create(:account) }
    let(:amount) { (0.5* sender.account_balance.to_i).to_i }
    it "performs transfer" do
      expect {
        described_class.perform_later sender.id
      }.to have_enqueued_job
    end
  end
end
