# frozen_string_literal: true

class Transaction < ApplicationRecord
  belongs_to :account
  enum transaction_status: [:pending, :transferred, :cancelled]
  validates_presence_of :reciever
  validates_presence_of :amount
  monetize :amount, as: "transfer_amount", allow_nil: false, numericality: {
    greater_than_or_equal_to: 0
  }
end
