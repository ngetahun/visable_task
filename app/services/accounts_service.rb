# frozen_string_literal: true

class AccountsService
  # class methods
  class << self
    def find(id)
      Account.find_by(id: id)
    end

    def fetch_last_n_transactions_for(account_id, n = 10)
      account = Account.find_by!(id: account_id)
      Transaction.where(reciever: account.account_number)
          .or(Transaction.where(account_id: account_id))
          .order(created_at: :desc)
          .limit(10)
          .to_a
    end

    def send_to(account_id, reciever_account_number, amount)
      account = Account.find_by(id: account_id)

      # error checking
      if account.account_number == reciever_account_number.to_i
        raise ArgumentError.new("Cannot send to yourself")
      end
      unless Account.exists?(account_number: reciever_account_number)
        raise ActiveRecord::RecordNotFound
      end
      if amount.to_i <= 0
        raise ArgumentError.new("Invalid transfer amount.")
      end
      account.send_to(reciever_account_number, amount.to_i)
    end
  end
end
