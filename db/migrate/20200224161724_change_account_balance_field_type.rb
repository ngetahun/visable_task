class ChangeAccountBalanceFieldType < ActiveRecord::Migration[5.2]
	def change
		change_column :accounts, :balance, :integer, limit: 8
  end
end
